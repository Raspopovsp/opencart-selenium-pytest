import pytest
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.firefox.service import Service as FirefoxService
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager

from utils import ConfigReader, DriverPath

sections = {
    'registration': 'registration info',
    'user_section': 'user info',
    'admin_section': 'admin info'
}

user_params = {
    'base_url': 'baseUrl',
    'username': 'test@test.com',
    'password': 'password'
}

base_url = ConfigReader.get_setting(ConfigReader.config_path, sections['user_section'], user_params['base_url'])


def pytest_addoption(parser):
    parser.addoption('--browser', '-B', action='store', default='firefox')
    parser.addoption('--url', '-U', action='store', default=base_url)
    parser.addoption('--local', '-L', action='store', default='true')


@pytest.fixture
def driver(request):
    driver = None
    browser = request.config.getoption('--browser')
    local = request.config.getoption('--local')
    if browser == 'chrome':
        if local == 'true':
            #driver_path = DriverPath.get_driver('chromedriver')
            service = ChromeService(ChromeDriverManager().install())
            driver = webdriver.Chrome(service=service)
        if local == 'false':
            chrome_options = webdriver.ChromeOptions()
            chrome_options.add_argument("--headless")
            driver = webdriver.Remote(
                command_executor='http://selenium__standalone-chrome:4444/wd/hub',
                #desired_capabilities=DesiredCapabilities.CHROME,
                options=chrome_options)
    elif browser == 'firefox':
        if local == 'true':
            #driver_path = DriverPath.get_driver('geckodriver')
            service = FirefoxService(GeckoDriverManager().install())
            driver = webdriver.Firefox(service=service)
        if local == 'false':
            firefox_options = webdriver.FirefoxOptions()
            firefox_options.add_argument('--headless')
            driver = webdriver.Remote(
                command_executor='http://selenium__standalone-firefox:4444/wd/hub',
                #desired_capabilities = DesiredCapabilities.FIREFOX,
                options=firefox_options)
    else:
        raise Exception(f'{browser} is not supported')

    driver.implicitly_wait(5)
    driver.maximize_window()
    driver.get(request.config.getoption('--url'))

    request.addfinalizer(driver.close)
    return driver
