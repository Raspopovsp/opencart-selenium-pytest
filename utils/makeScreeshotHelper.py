from PageObjects import BasePage
from pathlib import Path

base_path = Path.cwd()


class ScreenshotHelper(BasePage):

    def get_base_screen(self, folder, name):
        full_path = ''.join((str(base_path), folder, name))
        self.driver.save_screenshot(full_path)
