import os
import platform

os_name = platform.system()


class DriverPath:
    @staticmethod
    def get_driver(driver):
        ext = ''
        if os_name == 'Windows':
            ext = '.exe'
        if os_name == 'Linux':
            ext = '_linux'
        if os_name == 'Darwin':
            ext = '_mac'
        return os.path.abspath(f'drivers/{driver}{ext}')