# selenium-pytest-demo

**UI tests for OpenCart web-demo**
_В архиве имеются версии драйверов Chrome и Firefox для Mac, Linux, Windows который определяется и подключается автоматически, по умолчанию используется Firefox.

Драйвер тянется через менеджер.
Так же тесты частично провалятся, если в магазине временно закончился товар, который используется в кейсе(обновляется каждые 30 минут)._

Для запуска с генерацией отчета:
1) git clone https://gitlab.com/Raspopovsp/opencart-selenium-pytest.git
2) cd opencart-selenium-pytest
3) python3 -m venv env
4) source env/bin/activate
5) pip install -r requirements.txt
6) pytest -s --html=reports/report.html --capture=tee-sys

Тесты:
1) Тест поиска на главной странице с запросами: 'iphone','30'
2) Тест поиска на странице "Поиск" с запросом 'iMac' в разделе 'Desktop' с флагом "Поиск в подкатегориях" (Поиск работает)
3) Тест поиска на странице "Поиск" с запросом 'iMac' в разделе 'Desktop' с выключенным флагом "Поиск в подкатегориях"(Продукт не находится)
4) e2e тест "Заказ". Поиск продукта "Nikon D300", заказ 2-х экземпляров, подтверждение данных доставки, заказ.
    
