from PageObjects import BasePage


class Locators:
    quantity_input = '#input-quantity'
    add_to_cart_btn = '#button-cart'
    alert_success = '.alert-success'


class ProductPage(BasePage):
    def add_to_cart(self, quantity):
        self._fill_text_field(Locators.quantity_input, quantity)
        self._click_element(Locators.add_to_cart_btn)

    def get_alert_text(self):
        return self._get_element_text(Locators.alert_success)

