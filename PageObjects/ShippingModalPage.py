from PageObjects import BasePage


class Locators:
    modal_page = '#modal-shipping'
    flat_rate_radio = '.radio > label'
    apply_btn = '#button-shipping'


class ShippingModalPage(BasePage):
    def get_modal(self):
        if self._find_element(Locators.modal_page):
            return True
        else:
            return False

    def select_rate_radio_btn(self):
        self._click_element(Locators.flat_rate_radio)

    def apply_shipping(self):
        self._click_element(Locators.apply_btn)

    def wait_for_modal_closed(self):
        self._wait_for_invisible(Locators.modal_page)


