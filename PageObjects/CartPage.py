import time

from PageObjects import BasePage


class Locators:
    panel_group = '#accordion'
    panel = panel_group + '> .panel'
    panel_expand_toggle_link = 'a.accordion-toggle'

    country_select = '#input-country'
    region_select = '#input-zone'
    postcode = '#input-postcode'
    quote_btn = '#button-quote'
    checkout_btn = '.buttons.clearfix > .pull-right'

    shipping_modal = '#modal-shipping'


class CartPage(BasePage):

    def open_panel(self, value):
        self._find_and_click_by_title(Locators.panel, Locators.panel_expand_toggle_link, value)

    def get_shipping_estimate(self, country, region, postcode):
        self._select(Locators.country_select, country)
        self._select(Locators.region_select, region)
        self._fill_text_field(Locators.postcode, postcode)
        self._click_element(Locators.quote_btn)

    def checkout(self):
        self._click_element(Locators.checkout_btn)
