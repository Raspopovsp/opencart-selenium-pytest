from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException

import utils.basicLogger as bl


class BasePage:

    log = bl.loggen()

    def __init__(self, driver, wait=3):
        self.driver = driver
        self.wait = WebDriverWait(driver, wait)

    def _get_current_url(self):
        return self.driver.current_url

    def _get_page_title(self):
        return self.driver.title

    def _goto_url(self, url):
        self.driver.get(url)

    def _wait_for_visible(self, selector):
        return self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, selector)))

    def _wait_for_all_visible(self, selector):
        return self.wait.until(EC.visibility_of_all_elements_located((By.CSS_SELECTOR, selector)))

    def _wait_for_invisible(self, selector):
        return self.wait.until(EC.invisibility_of_element_located((By.CSS_SELECTOR, selector)))

    def _wait_element_to_be_clickable(self, selector):
        return self.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, selector)))

    def _wait_for_text_in_element(self, selector, text):
        self.wait.until(EC.text_to_be_present_in_element((By.CSS_SELECTOR, selector), text))

    def _find_element(self, selector):
        elem = None
        try:
            elem = self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR, selector)))
            self.log.info(f'selector: {selector} found ')
        except NoSuchElementException:
            self.log.info(f'selector: {selector} not found')
        return elem
            

    def _find_elements_list(self, selector):
        return self.wait.until(EC.presence_of_all_elements_located((By.CSS_SELECTOR, selector)))

    def _find_by_xpath(self, selector):
        return self.wait.until(EC.presence_of_element_located((By.XPATH, selector)))

    def _click_element(self, selector, index=0):
        elem = self._wait_for_all_visible(selector)[index]
        elem.click()

    # def _fill_text_field(self, selector, value):
    #     elem = self._find_element(selector)
    #     value_len = len(elem.get_property('value'))
    #     ActionChains(self.driver).move_to_element(elem).click().\
    #         send_keys(Keys.BACKSPACE * value_len).send_keys(value).perform()

    def _fill_text_field(self, selector, value):
        elem = self._wait_for_visible(selector)
        elem.click()
        elem.clear()
        elem.send_keys(value)

    def _get_element_text(self, selector):
        return self._find_element(selector).text

    def _get_element_property(self, selector, prop):
        return self._find_element(selector).get_attribute(prop)

    def _get_elements_counter(self, selector):
        return len(self._find_elements_list(selector))

    # в качестве "elem_title" надо передавать, только локатор элемента'
    def _find_and_click_by_title(self, elem, elem_title, value):
        elements = self._find_elements_list(elem)
        for elem in elements:
            if value in elem.find_element_by_css_selector(elem_title).text:
                elem.find_element_by_css_selector(elem_title).click()
                break

    def _select(self, selector, option, index=0):
        elem = self._wait_for_all_visible(selector)[index]
        select = Select(elem)
        if isinstance(option, int):
            select.select_by_index(option)
        if isinstance(option, str) and not option.isdigit():
            select.select_by_visible_text(option)
        if isinstance(option, str) and str(option).isdigit():
            select.select_by_value(str(option))

    """ Печать аттрибутов элемента """
    @staticmethod
    def get_element_attributes(elem):
        attrs = []
        for attr in elem.get_property('attributes'):
            attrs.append([attr['name'], attr['value']])
        return attrs
