from PageObjects import BasePage


class Locators:
    product_card = '.product-layout'
    product_card_title = '.caption > h4 > a'


class MainPage(BasePage):
    def get_current_url(self):
        return self._get_current_url()

    def title(self):
        return self._get_page_title()

    def choose_product(self, value):
        elem = self._find_and_click_by_title(Locators.product_card,
                                             Locators.product_card_title, value)
        return elem
