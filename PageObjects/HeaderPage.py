from PageObjects import BasePage


class Locators:
    header_search_block = '#search'
    header_search_input = header_search_block + '>.form-control'
    header_search_btn = header_search_block + '>.input-group-btn'
    cart_dropdown = '#cart > button'
    view_cart_link = '.dropdown-menu p.text-right > a'

    language_form = '#form-language'
    language_menu = language_form + '> .btn-link.dropdown-toggle'
    languages = {
        'RU': 'button[name="ru-ru"]',
        'EN': 'button[name="en-gb"]'
    }


class HeaderPage(BasePage):

    def change_language(self, locale):
        self._click_element(Locators.language_form)
        self._click_element(Locators.languages[locale])

    def search_for(self, value):
        self._fill_text_field(Locators.header_search_input, value)
    
    def search(self):
        self._click_element(Locators.header_search_btn)

    def view_cart(self):
        self._click_element(Locators.cart_dropdown)
        self._click_element(Locators.view_cart_link)