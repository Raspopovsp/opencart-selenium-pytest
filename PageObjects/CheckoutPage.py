import time

from selenium.webdriver.support.select import Select

from PageObjects import BasePage
from utils import FileReader


class Locators:
    panel_group = '#accordion'
    radio_block = '.radio'
    radio_btn = '.radio > label'
    account_btn = '#button-account'
    panel_toggle_link = 'a.accordion-toggle'

    personal_data_field_set = '#account'
    first_name_input = personal_data_field_set + ' input[name="firstname"]'
    last_name_input = personal_data_field_set + ' input[name="lastname"]'
    email_input = personal_data_field_set + ' input[name="email"]'
    telephone_input = personal_data_field_set + ' input[name="telephone"]'

    address_data_field_set = '#address'
    company_name_input = address_data_field_set + ' input[name="company"]'
    address_1_input = address_data_field_set + ' input[name="address_1"]'
    address_2_input = address_data_field_set + ' input[name="address_2"]'
    city_name_input = address_data_field_set + ' input[name="city"]'
    postcode_input = address_data_field_set + ' input[name="postcode"]'
    country_id = address_data_field_set + ' select[name="country_id"]'
    zone_id = address_data_field_set + ' select[name="zone_id"]'
    billing_btn = '#button-guest'
    terms_agree_chkbox = 'input[name="agree"]'

    # TODO Вынести кейворды в константы.
    accordion_panels = {
        'Способ оформления заказа': '#collapse-checkout-option',
        'Платежная информация': '#collapse-payment-address',
        'Адрес доставки': '#collapse-shipping-address',
        'Способ доставки': '#collapse-shipping-method',
        'Способ оплаты': '#collapse-payment-method',
        'Подтверждение заказа': '#collapse-checkout-confirm',
    }

    # TODO Вынести кейворды в константы.
    checkout_btns = {
        'account-btn': '#button-account',
        'guest-billing-btn': '#button-guest',
        'guest-shipping-btn': '#button-guest-shipping',
        'shipping-method-btn': '#button-shipping-method',
        'payment-method-btn': '#button-payment-method',
        'confirm-order-btn': '#button-confirm',
        'finish_checkout': 'a[href="http://demo-opencart.ru/index.php?route=common/home"]'
    }

    shipping_method_comment_area = accordion_panels['Способ доставки'] + ' textarea[name="comment"]'
    payment_method_comment_area = accordion_panels['Способ оплаты'] + ' textarea[name="comment"]'

    success_message = '#common-success #content > h1'


class CheckoutPage(BasePage):

    def get_page_title(self):
        return self._get_page_title()

    def get_current_url(self):
        return self._get_current_url()

    def click_continue(self, btn):
        self._click_element(Locators.checkout_btns[btn])

    def guest_checkout(self):
        self._click_element(Locators.radio_btn, index=1)
        self._click_element(Locators.account_btn)

    def fill_personal_info(self, value):
        customer_data = FileReader.parse_customer_data(value)
        self._fill_text_field(Locators.first_name_input, customer_data['first_name'])
        self._fill_text_field(Locators.last_name_input, customer_data['last_name'])
        self._fill_text_field(Locators.email_input, customer_data['email'])
        self._fill_text_field(Locators.telephone_input, customer_data['phone'])

    def fill_address_info(self, value):
        customer_data = FileReader.parse_customer_data(value)
        self._fill_text_field(Locators.company_name_input, customer_data['company'])
        self._fill_text_field(Locators.address_1_input, customer_data['address_1'])
        self._fill_text_field(Locators.address_2_input, customer_data['address_2'])
        self._fill_text_field(Locators.city_name_input, customer_data['city'])

    def apply_billing(self):
        self._click_element(Locators.billing_btn)

    def shipping_comment(self, value):
        self._fill_text_field(Locators.shipping_method_comment_area, value)

    def terms_checkbox_agree(self):
        self._click_element(Locators.terms_agree_chkbox)

    def confirm_order(self):
        self._click_element(Locators.checkout_btns['confirm-order-btn'])

    def success_message(self):
        return self._get_element_text(Locators.success_message)
