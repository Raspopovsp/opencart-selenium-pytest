from PageObjects import BasePage

search_url = 'http://demo-opencart.ru/index.php?route=product/search'


class Locators:
    product_search_container = '#product-search'
    product_card = '.product-layout.product-grid'
    product_card_title = product_card + ' h4'
    content_block = '#content'
    content_header = content_block + '> h1'

    search_input = '#input-search'
    search_category_select = 'select[name="category_id"]'
    search_in_subcategory_flag = 'input[name="sub_category"]'
    search_btn = '#button-search'
    empty_search_msg = '//DIV[@id="content"]//P[text()="Your shopping cart is empty!"]'


class SearchPage(BasePage):

    def goto_search_page(self):
        self._goto_url(search_url)

    def get_search_result_counter(self):
        return self._get_elements_counter(Locators.product_card)

    def get_header_text(self):
        return self._get_element_text(Locators.content_header)

    def search_for(self, value):
        self._fill_text_field(Locators.search_input, value)

    def select_category(self, value):
        self._select(Locators.search_category_select, value)

    def check_search_in_subcategory_flag(self):
        self._click_element(Locators.search_in_subcategory_flag)

    def search(self):
        self._click_element(Locators.search_btn)

    def get_product_card_title(self):
        return self._get_element_text(Locators.product_card_title)

    def get_empty_search_result_msg(self):
        return self._find_by_xpath(Locators.empty_search_msg).text
