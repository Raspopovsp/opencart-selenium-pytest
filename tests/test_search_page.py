from PageObjects import SearchPage
#from utils import LogGenerator
import utils.basicLogger as bl

#logger = LogGenerator.loggen()

log = bl.loggen()


''' Поиск в родительской категории с флагом "Search in subcategories" '''
def test_search_in_subcategory_with_sub_flag(driver):
    search_page = SearchPage(driver)


    search_page.goto_search_page()
    search_page.search_for('iMac')
    search_page.select_category('Desktops')
    search_page.check_search_in_subcategory_flag()
    search_page.search()
    product_card_title = search_page.get_product_card_title()
    log.info(f'product card title: {product_card_title}')
    assert product_card_title == 'iMac'


''' Поиск в родительской категории без флага "Search in subcategories" '''
def test_search_in_subcategory_without_flag(driver):
    search_page = SearchPage(driver)
    search_page.goto_search_page()
    search_page.search_for('iMac')
    search_page.select_category('Desktops')
    search_page.search()
    assert 'Your shopping cart is empty!' == search_page.get_empty_search_result_msg()
