import time
from PageObjects import MainPage, HeaderPage, ProductPage, CartPage, ShippingModalPage, CheckoutPage
from utils import ScreenshotHelper

import utils.basicLogger as bl

log = bl.loggen()

def test_checkout_e2e(driver):
    screenshot_dir = '/reports/screenshots/'
    screen_helper = ScreenshotHelper(driver)

    main_page = MainPage(driver)
    main_header = HeaderPage(driver)
    product_page = ProductPage(driver)
    cart_page = CartPage(driver)
    shipping_modal_page = ShippingModalPage(driver)
    checkout_page = CheckoutPage(driver)

    main_header.change_language('RU')

    # product = 'iPhone'
    product = 'Nikon D300'

    main_header.search_for(product)
    main_header.search()

    main_page.choose_product(product)
    log.info(f'success: {product} page')

    product_page.add_to_cart(quantity=2)
    log.info(f'success: {product} added to cart')

    main_header.view_cart()
    log.info(f'success: Shopping Card redirect')

    cart_page.open_panel('Расчет стоимости доставки')

    cart_page.get_shipping_estimate('Великобритания', 'Carmarthenshire', 111255)
    log.info('success: Shipping modal page opened')

    shipping_modal_page.select_rate_radio_btn()
    shipping_modal_page.apply_shipping()
    shipping_modal_page.wait_for_modal_closed()
    cart_page.checkout()
    log.info('success: Checkout redirect')

    checkout_page.guest_checkout()
    checkout_page.fill_personal_info('customer')
    checkout_page.fill_address_info('customer')
    checkout_page.click_continue('guest-billing-btn')
    checkout_page.shipping_comment('Тестовый комментарий к отправке')
    checkout_page.click_continue('shipping-method-btn')

    # driver.execute_script('''
    #     let payment_panel = document.getElementById('collapse-payment-method')
    #     payment_panel.scrollIntoView();
    # ''')

    checkout_page.terms_checkbox_agree()
    checkout_page.click_continue('payment-method-btn')
    checkout_page.confirm_order()

    if 'Ваш заказ принят!' == checkout_page.success_message():
        log.info('success: Order has been placed')
        assert True
    else:
        log.error('order confirm fail')
        assert False, screen_helper.get_base_screen(screenshot_dir, 'order_confirm_fail.png')

    checkout_page.click_continue('finish_checkout')
    if 'route=common/home' in checkout_page.get_current_url():
        log.info('Order confirmed. Redirect to index')
    else:
        log.error('Redirect to index fail')
