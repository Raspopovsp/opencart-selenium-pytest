from PageObjects import MainPage
#from utils import LogGenerator

#logger = LogGenerator.loggen()
import utils.basicLogger as bl


log = bl.loggen()

def test_main_page_url(driver):
    log.info('main page successful')
    assert 'http://demo-opencart.ru/' == MainPage(driver).get_current_url()
