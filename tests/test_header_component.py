import pytest
from PageObjects import SearchPage, HeaderPage
from utils import ScreenshotHelper as SH
#from utils import LogGenerator
#logger = LogGenerator.loggen()
import utils.basicLogger as bl

log = bl.loggen()


@pytest.mark.parametrize('text_input, value', [('iphone', 1), ('30', 3)])
def test_search_field(driver, text_input, value):
    header_component = HeaderPage(driver)
    search_page = SearchPage(driver)
    header_component.search_for(text_input)
    header_component.search()
    log.info(f'Header: {search_page.get_header_text()}')
    log.info(f'Sourch results: {search_page.get_search_result_counter()}')
    SH(driver).get_base_screen(folder='/reports/screenshots/', 
        name=(f'search_{text_input}.png'))
    assert text_input in search_page.get_header_text()
    assert value == search_page.get_search_result_counter()
